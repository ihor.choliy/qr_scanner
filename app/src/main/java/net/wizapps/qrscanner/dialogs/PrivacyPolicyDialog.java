package net.wizapps.qrscanner.dialogs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.base.BaseDialog;
import net.wizapps.qrscanner.tools.Constants;

import butterknife.BindView;
import butterknife.OnClick;

public class PrivacyPolicyDialog extends BaseDialog {

    @BindView(R.id.progressBar) View mProgress;
    @BindView(R.id.webView) WebView mWebView;

    @Override
    protected int layoutRes() {
        return R.layout.dialog_privacy_policy;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        mWebView.loadUrl(getString(R.string.url_privacy_policy));
        mWebView.getSettings().setJavaScriptEnabled(Boolean.TRUE);
        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (newProgress == Constants.ONE_HUNDRED_PERCENT) {
                    mProgress.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    @OnClick(R.id.buttonClose)
    public void onClick() {
        dismiss();
    }
}