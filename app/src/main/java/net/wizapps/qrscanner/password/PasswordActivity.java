package net.wizapps.qrscanner.password;

import android.content.Intent;
import android.text.TextUtils;
import android.widget.TextView;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.base.BaseActivity;
import net.wizapps.qrscanner.callbacks.OnNumberClickListener;
import net.wizapps.qrscanner.callbacks.PassAnimationListener;
import net.wizapps.qrscanner.history.HistoryActivity;
import net.wizapps.qrscanner.password.mvp.PasswordPresenter;
import net.wizapps.qrscanner.password.mvp.PasswordPresenterImpl;
import net.wizapps.qrscanner.password.mvp.PasswordView;
import net.wizapps.qrscanner.tools.Constants;
import net.wizapps.qrscanner.tools.Events;
import net.wizapps.qrscanner.utils.InfoUtils;
import net.wizapps.qrscanner.utils.PassUtils;
import net.wizapps.qrscanner.utils.PrefUtils;
import net.wizapps.qrscanner.views.NumbersView;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;
import icepick.State;

public class PasswordActivity extends BaseActivity implements
        OnNumberClickListener,
        PassAnimationListener,
        PasswordView {

    @BindView(R.id.info) TextView mInfo;
    @BindView(R.id.pass) TextView mPass;
    @BindView(R.id.numbers) NumbersView mNumbers;

    @State String mEnteredPass = Constants.EMPTY;
    @State String mTemporaryPass = Constants.EMPTY;

    private PasswordPresenter mPresenter = new PasswordPresenterImpl();
    private PassUtils mPassUtils;

    @Override
    protected int layoutRes() {
        return R.layout.activity_password;
    }

    @Override
    protected void setUi() {
        mPassUtils = new PassUtils(this, this, mInfo, mPass);
        mNumbers.setOnNumberClickListener(this);
        if (!TextUtils.isEmpty(mTemporaryPass)) {
            mInfo.setText(R.string.pass_repeat);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.bind(this);
        mPresenter.getEncryptedPass(mEnteredPass);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.unbind();
    }

    @Override
    protected void onDestroy() {
        mPassUtils.clearInstance();
        super.onDestroy();
    }

    @Override
    public void onNumberClick(int number) {
        if (mEnteredPass.length() < Constants.PASSWORD_LENGTH) {
            mEnteredPass += String.valueOf(number);
            mPresenter.getEncryptedPass(mEnteredPass);
            checkLogic();
        }
    }

    @Override
    public void onClearClick() {
        if (mEnteredPass.length() > Constants.ZERO) {
            mEnteredPass = mEnteredPass.substring(Constants.ZERO, mEnteredPass.length() - 1);
            mPresenter.getEncryptedPass(mEnteredPass);
        }
    }

    @Override
    public void encryptedPassReady(String encryptedPass) {
        mPass.setText(encryptedPass);
    }

    @Override
    public void onSuccessAnimEnd(boolean passwordAdded) {
        if (passwordAdded) {
            EventBus.getDefault().postSticky(new Events.PasswordConditionEvent(Boolean.TRUE));
            PrefUtils.setPassword(this, mEnteredPass);
            finish();
        } else {
            finish();
            startActivity(new Intent(this, HistoryActivity.class));
        }
    }

    @Override
    public void onErrorAnimEnd() {
        mEnteredPass = Constants.EMPTY;
    }

    @Override
    public void onRepeatAnimEnd() {
        mTemporaryPass = mEnteredPass;
        mEnteredPass = Constants.EMPTY;
    }

    @OnClick(R.id.backButton)
    public void onClick() {
        onBackClick();
    }

    private void checkLogic() {
        if (mEnteredPass.length() == Constants.PASSWORD_LENGTH) {
            String password = PrefUtils.getPassword(this);
            if (!TextUtils.isEmpty(password)) {
                enterPassLogic(password);
            } else {
                setPassLogic();
            }
        }
    }

    private void enterPassLogic(String password) {
        if (mEnteredPass.equals(password)) {
            mPassUtils.startSuccessAnim(Boolean.FALSE);
        } else {
            showError(R.string.pass_error);
        }
    }

    private void setPassLogic() {
        if (!TextUtils.isEmpty(mTemporaryPass)) {
            if (mEnteredPass.equals(mTemporaryPass)) {
                mPassUtils.startSuccessAnim(Boolean.TRUE);
            } else {
                showError(R.string.pass_error_match);
            }
        } else {
            mPassUtils.startRepeatAnim();
        }
    }

    private void showError(int errorTextResId) {
        mPassUtils.startErrorAnim();
        InfoUtils.showAlertError(this, errorTextResId);
    }
}