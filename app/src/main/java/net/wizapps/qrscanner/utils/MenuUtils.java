package net.wizapps.qrscanner.utils;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.dialogs.AboutDialog;
import net.wizapps.qrscanner.dialogs.PrivacyPolicyDialog;
import net.wizapps.qrscanner.donate.DonateActivity;
import net.wizapps.qrscanner.history.HistoryActivity;
import net.wizapps.qrscanner.password.PasswordActivity;
import net.wizapps.qrscanner.settings.SettingsActivity;
import net.wizapps.qrscanner.tools.Constants;

import java.util.Calendar;

public final class MenuUtils {

    private MenuUtils() {}

    public static void setupDate(Context context, NavigationView view) {
        View header = view.getHeaderView(Constants.ZERO);
        TextView developer = header.findViewById(R.id.drawerTextDeveloper);
        int year = Calendar.getInstance().get(Calendar.YEAR);
        String text = context.getString(R.string.text_developer, String.valueOf(year));
        developer.setText(text);
    }

    public static void onNavDrawerClicked(Context context, int id) {
        switch (id) {
            case R.id.navHistory:
                showHistory(context);
                break;
            case R.id.navSettings:
                showSettings(context);
                break;
            case R.id.navDonate:
                showDonate(context);
                break;
            case R.id.navAbout:
                showAbout(context);
                break;
            case R.id.navShare:
                IntentUtils.shareIntent((AppCompatActivity) context);
                break;
            case R.id.navEmail:
                IntentUtils.emailIntent(context);
                break;
            case R.id.navFeedback:
                IntentUtils.feedbackIntent(context);
                break;
            case R.id.navApps:
                IntentUtils.webIntent(context, R.string.url_apps);
                break;
            case R.id.navPrivacy:
                showPrivacyPolicy(context);
                break;
        }
    }

    private static void showSettings(Context context) {
        startActivity(context, SettingsActivity.class);
    }

    private static void showHistory(Context context) {
        if (TextUtils.isEmpty(PrefUtils.getPassword(context))) {
            startActivity(context, HistoryActivity.class);
        } else {
            startActivity(context, PasswordActivity.class);
        }
    }

    public static void showDonate(Context context) {
        startActivity(context, DonateActivity.class);
    }

    private static void showAbout(Context context) {
        new AboutDialog().show(getFragmentManager(context), AboutDialog.class.getSimpleName());
    }

    private static void showPrivacyPolicy(Context context) {
        new PrivacyPolicyDialog().show(getFragmentManager(context), PrivacyPolicyDialog.class.getSimpleName());
    }

    private static void startActivity(Context context, Class<?> cls) {
        context.startActivity(new Intent(context, cls));
    }

    private static FragmentManager getFragmentManager(Context context) {
        return ((AppCompatActivity) context).getSupportFragmentManager();
    }
}