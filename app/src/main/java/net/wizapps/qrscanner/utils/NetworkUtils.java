package net.wizapps.qrscanner.utils;

import android.content.Context;
import android.net.ConnectivityManager;

public final class NetworkUtils {

    private NetworkUtils() {}

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            return cm.getActiveNetworkInfo() != null;
        } else {
            return Boolean.FALSE;
        }
    }
}