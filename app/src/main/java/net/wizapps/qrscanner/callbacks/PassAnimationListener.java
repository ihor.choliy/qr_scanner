package net.wizapps.qrscanner.callbacks;

public interface PassAnimationListener {

    void onSuccessAnimEnd(boolean passwordAdded);

    void onErrorAnimEnd();

    void onRepeatAnimEnd();
}