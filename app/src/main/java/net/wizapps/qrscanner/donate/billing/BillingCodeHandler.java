package net.wizapps.qrscanner.donate.billing;

import com.android.billingclient.api.BillingClient;

public final class BillingCodeHandler {

    private BillingCodeHandler() {}

    public static String handleResponseCode(int responseCode) {
        switch (responseCode) {
            case BillingClient.BillingResponse.OK:
                return BillingConstants.SUCCESS;
            case BillingClient.BillingResponse.FEATURE_NOT_SUPPORTED:
                return BillingConstants.FEATURE_NOT_SUPPORTED;
            case BillingClient.BillingResponse.SERVICE_DISCONNECTED:
                return BillingConstants.SERVICE_DISCONNECTED;
            case BillingClient.BillingResponse.USER_CANCELED:
                return BillingConstants.USER_CANCELED;
            case BillingClient.BillingResponse.SERVICE_UNAVAILABLE:
                return BillingConstants.SERVICE_UNAVAILABLE;
            case BillingClient.BillingResponse.BILLING_UNAVAILABLE:
                return BillingConstants.BILLING_UNAVAILABLE;
            case BillingClient.BillingResponse.ITEM_UNAVAILABLE:
                return BillingConstants.ITEM_UNAVAILABLE;
            case BillingClient.BillingResponse.DEVELOPER_ERROR:
                return BillingConstants.DEVELOPER_ERROR;
            case BillingClient.BillingResponse.ERROR:
                return BillingConstants.ERROR;
            case BillingClient.BillingResponse.ITEM_ALREADY_OWNED:
                return BillingConstants.ITEM_ALREADY_OWNED;
            default:
                return BillingConstants.ITEM_NOT_OWNED;
        }
    }
}