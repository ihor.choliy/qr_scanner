package net.wizapps.qrscanner.dialogs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.base.BaseDialog;
import net.wizapps.qrscanner.data.ScanLab;
import net.wizapps.qrscanner.data.ScanModel;
import net.wizapps.qrscanner.tools.Constants;
import net.wizapps.qrscanner.tools.Events;
import net.wizapps.qrscanner.utils.DateTimeUtils;
import net.wizapps.qrscanner.utils.InfoUtils;
import net.wizapps.qrscanner.utils.ResultUtils;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;

public class InfoDialog extends BaseDialog implements TextView.OnEditorActionListener {

    @BindView(R.id.nameLayout) View mNameLayout;
    @BindView(R.id.buttonCopy) View mCopyButton;
    @BindView(R.id.textName) EditText mName;
    @BindView(R.id.textType) TextView mType;
    @BindView(R.id.textFormat) TextView mFormat;
    @BindView(R.id.textDate) TextView mDate;
    @BindView(R.id.textContent) TextView mContent;

    private ScanModel mModel;

    public static InfoDialog newInstance(ScanModel model, boolean isHistory) {
        Bundle args = new Bundle();
        args.putParcelable(Constants.KEY_RESULT, model);
        args.putBoolean(Constants.KEY_IS_HISTORY, isHistory);
        InfoDialog dialog = new InfoDialog();
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    protected int layoutRes() {
        return R.layout.dialog_info;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            setNameField(getArguments().getBoolean(Constants.KEY_IS_HISTORY));
            setData((ScanModel) getArguments().getParcelable(Constants.KEY_RESULT));
        }
    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            mName.setCursorVisible(Boolean.FALSE);
            mModel.setName(mName.getText().toString());
            ScanLab.getInstance(getActivity()).updateItem(mModel);
            EventBus.getDefault().post(new Events.UpdateHistoryEvent());
            InfoUtils.showInfo(getActivity(), R.string.text_name_info);
        }
        return Boolean.FALSE;
    }

    @OnClick({R.id.textName, R.id.buttonClose, R.id.buttonCopy})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.textName:
                onNameClicked();
                break;
            case R.id.buttonClose:
                dismiss();
                break;
            case R.id.buttonCopy:
                onCopyClicked();
                break;
        }
    }

    private void setNameField(boolean isHistory) {
        if (isHistory) {
            mName.setOnEditorActionListener(this);
        } else {
            mNameLayout.setVisibility(View.GONE);
            mCopyButton.setVisibility(View.GONE);
        }
    }

    private void setData(ScanModel model) {
        mModel = model;
        if (mModel != null) {
            if (!TextUtils.isEmpty(mModel.getName())) mName.setText(mModel.getName());
            mType.setText(mModel.getType());
            mFormat.setText(mModel.getFormat());
            mDate.setText(DateTimeUtils.getFormattedDate(getActivity(), mModel.getDate()));
            mContent.setText(mModel.getContent());
        }
    }

    private void onNameClicked() {
        mName.setCursorVisible(Boolean.TRUE);
        if (!TextUtils.isEmpty(mName.getText().toString())) {
            mName.setSelection(mName.getText().toString().length());
        }
    }

    private void onCopyClicked() {
        ResultUtils.copyResult(getActivity(), mContent.getText().toString(), Boolean.FALSE);
        InfoUtils.showAlertInfo(getActivity(), R.string.text_data_copied);
        dismiss();
    }
}