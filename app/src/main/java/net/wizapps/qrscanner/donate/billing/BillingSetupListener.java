package net.wizapps.qrscanner.donate.billing;

public interface BillingSetupListener {

    void onConnectionSuccessful();

    void onConnectionFailed(int responseCode);
}