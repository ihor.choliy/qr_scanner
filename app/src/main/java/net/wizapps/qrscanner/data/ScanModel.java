package net.wizapps.qrscanner.data;

import android.os.Parcel;
import android.os.Parcelable;

import net.wizapps.qrscanner.tools.Constants;

public class ScanModel implements Parcelable {

    private String mId;
    private String mName;
    private String mType;
    private String mFormat;
    private Long mDate;
    private String mContent;

    public ScanModel(String name, String type, String format, Long date, String content) {
        setName(name);
        setType(type);
        setFormat(format);
        setDate(date);
        setContent(content);
    }

    public ScanModel(String id, String name, String type, String format, Long date, String content) {
        this(name, type, format, date, content);
        setId(id);
    }

    protected ScanModel(Parcel in) {
        mId = in.readString();
        mName = in.readString();
        mType = in.readString();
        mFormat = in.readString();
        if (in.readByte() == Constants.ZERO) {
            mDate = null;
        } else {
            mDate = in.readLong();
        }
        mContent = in.readString();
    }

    public static final Creator<ScanModel> CREATOR = new Creator<ScanModel>() {
        @Override
        public ScanModel createFromParcel(Parcel in) {
            return new ScanModel(in);
        }

        @Override
        public ScanModel[] newArray(int size) {
            return new ScanModel[size];
        }
    };

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mId);
        parcel.writeString(mName);
        parcel.writeString(mType);
        parcel.writeString(mFormat);
        if (mDate == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeLong(mDate);
        }
        parcel.writeString(mContent);
    }

    @Override
    public int describeContents() {
        return Constants.ZERO;
    }

    public String getId() {
        return mId;
    }

    private void setId(String id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getType() {
        return mType;
    }

    private void setType(String type) {
        mType = type;
    }

    public String getFormat() {
        return mFormat;
    }

    private void setFormat(String format) {
        mFormat = format;
    }

    public Long getDate() {
        return mDate;
    }

    private void setDate(Long date) {
        mDate = date;
    }

    public String getContent() {
        return mContent;
    }

    private void setContent(String content) {
        mContent = content;
    }
}