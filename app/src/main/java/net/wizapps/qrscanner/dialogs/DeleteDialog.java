package net.wizapps.qrscanner.dialogs;

import android.view.View;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.base.BaseDialog;
import net.wizapps.qrscanner.data.ScanLab;
import net.wizapps.qrscanner.tools.Events;

import org.greenrobot.eventbus.EventBus;

import butterknife.OnClick;

public class DeleteDialog extends BaseDialog {

    @Override
    protected int layoutRes() {
        return R.layout.dialog_delete;
    }

    @OnClick({R.id.buttonCancel, R.id.buttonYes})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonCancel:
                dismiss();
                break;
            case R.id.buttonYes:
                onYesClick();
                break;
        }
    }

    private void onYesClick() {
        ScanLab.getInstance(getActivity()).clearAllHistory();
        EventBus.getDefault().post(new Events.DeleteHistoryEvent());
        dismiss();
    }
}