package net.wizapps.qrscanner.password.mvp;

public interface PasswordView {

    void encryptedPassReady(String encryptedPass);
}