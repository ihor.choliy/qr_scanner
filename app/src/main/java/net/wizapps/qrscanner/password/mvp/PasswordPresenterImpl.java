package net.wizapps.qrscanner.password.mvp;

import net.wizapps.qrscanner.tools.Constants;

public class PasswordPresenterImpl implements PasswordPresenter {

    private PasswordView mView;

    @Override
    public void bind(PasswordView view) {
        mView = view;
    }

    @Override
    public void unbind() {
        mView = null;
    }

    @Override
    public void getEncryptedPass(String enteredPass) {
        StringBuilder encryptedPass = new StringBuilder();
        for (int i = Constants.ZERO; i < enteredPass.length(); i++) {
            encryptedPass.append(Constants.HIDDEN_PASS);
        }
        if (mView != null) mView.encryptedPassReady(encryptedPass.toString());
    }
}