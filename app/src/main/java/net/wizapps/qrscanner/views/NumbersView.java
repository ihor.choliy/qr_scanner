package net.wizapps.qrscanner.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.callbacks.OnNumberClickListener;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class NumbersView extends LinearLayout {

    private OnNumberClickListener mListener;

    public NumbersView(Context context) {
        super(context);
        init();
    }

    public NumbersView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    @OnClick({R.id.numberZero, R.id.numberOne, R.id.numberTwo, R.id.numberThree, R.id.numberFour, R.id.numberFive, R.id.numberSix, R.id.numberSeven, R.id.numberEight, R.id.numberNine, R.id.buttonClear})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.numberZero:
                mListener.onNumberClick(0);
                break;
            case R.id.numberOne:
                mListener.onNumberClick(1);
                break;
            case R.id.numberTwo:
                mListener.onNumberClick(2);
                break;
            case R.id.numberThree:
                mListener.onNumberClick(3);
                break;
            case R.id.numberFour:
                mListener.onNumberClick(4);
                break;
            case R.id.numberFive:
                mListener.onNumberClick(5);
                break;
            case R.id.numberSix:
                mListener.onNumberClick(6);
                break;
            case R.id.numberSeven:
                mListener.onNumberClick(7);
                break;
            case R.id.numberEight:
                mListener.onNumberClick(8);
                break;
            case R.id.numberNine:
                mListener.onNumberClick(9);
                break;
            case R.id.buttonClear:
                mListener.onClearClick();
                break;
        }
    }

    public void setOnNumberClickListener(OnNumberClickListener listener) {
        mListener = listener;
    }

    private void init() {
        View view = View.inflate(getContext(), R.layout.view_numbers, this);
        ButterKnife.bind(this, view);
    }
}