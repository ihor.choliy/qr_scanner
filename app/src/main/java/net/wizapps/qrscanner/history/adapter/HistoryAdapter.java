package net.wizapps.qrscanner.history.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.data.ScanModel;
import net.wizapps.qrscanner.tools.Constants;

import java.util.ArrayList;
import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryHolder> {

    private List<ScanModel> mHistory = new ArrayList<>();

    @NonNull
    @Override
    public HistoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_item_history, parent, Boolean.FALSE);
        return new HistoryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryHolder holder, int position) {
        holder.bindView(mHistory.get(position));
    }

    @Override
    public int getItemCount() {
        return mHistory == null ? Constants.ZERO : mHistory.size();
    }

    public List<ScanModel> removeHistory() {
        List<ScanModel> history = new ArrayList<>();
        history.addAll(mHistory);
        mHistory.clear();
        notifyDataSetChanged();
        return history;
    }

    public ScanModel removeItem(int position) {
        ScanModel item = mHistory.get(position);
        mHistory.remove(position);
        notifyItemRemoved(position);
        return item;
    }

    public void setHistory(List<ScanModel> history) {
        if (history != null) {
            mHistory.clear();
            mHistory.addAll(history);
            notifyDataSetChanged();
        }
    }

    public void restoreItem(int position, ScanModel model) {
        mHistory.add(position, model);
        notifyItemInserted(position);
    }
}