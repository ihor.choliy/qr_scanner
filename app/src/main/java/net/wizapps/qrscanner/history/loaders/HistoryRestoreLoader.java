package net.wizapps.qrscanner.history.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import net.wizapps.qrscanner.data.ScanLab;
import net.wizapps.qrscanner.data.ScanModel;

import java.util.List;

public class HistoryRestoreLoader extends AsyncTaskLoader<List<ScanModel>> {

    public static final int RESTORE_LOADER_ID = 222;
    private final List<ScanModel> mHistory;

    public HistoryRestoreLoader(Context context, List<ScanModel> history) {
        super(context);
        mHistory = history;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public List<ScanModel> loadInBackground() {
        ScanLab.getInstance(getContext()).restoreHistory(mHistory);
        return ScanLab.getInstance(getContext()).getHistory();
    }
}