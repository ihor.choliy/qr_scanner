package net.wizapps.qrscanner.donate.mvp;

import net.wizapps.qrscanner.donate.adapter.SkuModel;

import java.util.List;

public interface DonateView {

    void dataConverted(List<SkuModel> skuList);
}