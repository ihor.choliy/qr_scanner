package net.wizapps.qrscanner.scanner.mvp;

public class ScannerPresenterImpl implements ScannerPresenter {

    private ScannerView mView;

    @Override
    public void bind(ScannerView view) {
        mView = view;
    }

    @Override
    public void unbind() {
        mView = null;
    }

    @Override
    public void onFlashLightClicked(boolean isFlashLightOn) {
        mView.playFlashLightSound();
        mView.switchFlashLight(isFlashLightOn);
    }

    @Override
    public void checkFleshLight(boolean isFlashLightOn) {
        mView.switchFlashLight(isFlashLightOn);
    }

    @Override
    public void onBackPressed(boolean isDrawerOpen) {
        if (isDrawerOpen) {
            mView.closeNavDrawer();
        } else {
            mView.finishActivity();
        }
    }
}