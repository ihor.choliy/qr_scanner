package net.wizapps.qrscanner.callbacks;

import android.support.annotation.NonNull;
import android.support.v4.widget.DrawerLayout;
import android.view.View;

public abstract class DrawerListenerImpl implements DrawerLayout.DrawerListener {

    @Override
    public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {}

    @Override
    public void onDrawerStateChanged(int newState) {}
}