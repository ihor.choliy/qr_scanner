package net.wizapps.qrscanner.dialogs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.base.BaseDialog;
import net.wizapps.qrscanner.tools.Constants;
import net.wizapps.qrscanner.utils.InfoUtils;
import net.wizapps.qrscanner.utils.IntentUtils;
import net.wizapps.qrscanner.utils.ResultUtils;

import butterknife.BindView;
import butterknife.OnClick;

public class WalletDialog extends BaseDialog {

    @BindView(R.id.walletType) TextView mWalletType;
    @BindView(R.id.walletAddress) TextView mWalletAddress;

    public static WalletDialog newInstance(int typeResId, int addressResId, int iconResId) {
        Bundle args = new Bundle();
        args.putInt(Constants.KEY_WALLET_TYPE, typeResId);
        args.putInt(Constants.KEY_WALLET_ADDRESS, addressResId);
        args.putInt(Constants.KEY_WALLET_ICON, iconResId);
        WalletDialog fragment = new WalletDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int layoutRes() {
        return R.layout.dialog_wallet;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        if (getArguments() != null) {
            mWalletAddress.setText(getString(getArguments().getInt(Constants.KEY_WALLET_ADDRESS)));
            mWalletType.setText(getString(getArguments().getInt(Constants.KEY_WALLET_TYPE)));
            mWalletType.setCompoundDrawablesWithIntrinsicBounds(
                    getArguments().getInt(Constants.KEY_WALLET_ICON),
                    Constants.ZERO,
                    Constants.ZERO,
                    Constants.ZERO);
        }
    }

    @OnClick({R.id.buttonClose, R.id.buttonShare, R.id.buttonCopy})
    public void onClick(View view) {
        String wallet = mWalletAddress.getText().toString().trim();
        switch (view.getId()) {
            case R.id.buttonClose:
                dismiss();
                break;
            case R.id.buttonShare:
                IntentUtils.shareIntent(getActivity(), wallet);
                break;
            case R.id.buttonCopy:
                onCopyClick(wallet);
                break;
        }
    }

    private void onCopyClick(String wallet) {
        ResultUtils.copyResult(getActivity(), wallet, Boolean.FALSE);
        InfoUtils.showAlertInfo(getActivity(), R.string.donate_copied);
        dismiss();
    }
}