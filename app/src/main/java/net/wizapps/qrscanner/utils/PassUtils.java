package net.wizapps.qrscanner.utils;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.callbacks.AnimationListenerImpl;
import net.wizapps.qrscanner.callbacks.PassAnimationListener;
import net.wizapps.qrscanner.tools.Constants;

public final class PassUtils {

    private Context mContext;
    private TextView mInfoView;
    private TextView mPassView;
    private PassAnimationListener mListener;

    public PassUtils(Context context, PassAnimationListener listener, TextView infoView, TextView passView) {
        mContext = context;
        mInfoView = infoView;
        mPassView = passView;
        mListener = listener;
    }

    public void startSuccessAnim(final boolean passwordAdded) {
        final Animation increaseAnim = AnimationUtils.loadAnimation(mContext, R.anim.anim_scale_icrease);
        final Animation decreaseAnim = AnimationUtils.loadAnimation(mContext, R.anim.anim_scale_decrease);

        increaseAnim.setAnimationListener(new AnimationListenerImpl() {
            @Override
            public void onAnimationEnd(Animation animation) {
                mPassView.startAnimation(decreaseAnim);
            }
        });
        decreaseAnim.setAnimationListener(new AnimationListenerImpl() {
            @Override
            public void onAnimationEnd(Animation animation) {
                mListener.onSuccessAnimEnd(passwordAdded);
            }
        });

        mPassView.setTextColor(ContextCompat.getColor(mContext, R.color.colorAccent));
        mPassView.startAnimation(increaseAnim);
    }

    public void startErrorAnim() {
        final Animation leftAnim = AnimationUtils.loadAnimation(mContext, R.anim.anim_translate_left);
        final Animation rightAnim = AnimationUtils.loadAnimation(mContext, R.anim.anim_translate_right);
        final Animation centerAnim = AnimationUtils.loadAnimation(mContext, R.anim.anim_translate_center);
        final Animation hideAnim = AnimationUtils.loadAnimation(mContext, R.anim.anim_alpha_hide);

        leftAnim.setAnimationListener(new AnimationListenerImpl() {
            @Override
            public void onAnimationEnd(Animation animation) {
                mPassView.startAnimation(rightAnim);
            }
        });
        rightAnim.setAnimationListener(new AnimationListenerImpl() {
            @Override
            public void onAnimationEnd(Animation animation) {
                mPassView.startAnimation(centerAnim);
            }
        });
        centerAnim.setAnimationListener(new AnimationListenerImpl() {
            @Override
            public void onAnimationEnd(Animation animation) {
                mPassView.startAnimation(hideAnim);
            }
        });
        hideAnim.setAnimationListener(new AnimationListenerImpl() {
            @Override
            public void onAnimationEnd(Animation animation) {
                mPassView.setText(Constants.EMPTY);
                mPassView.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
                mListener.onErrorAnimEnd();
            }
        });

        mPassView.setTextColor(ContextCompat.getColor(mContext, R.color.colorRed));
        mPassView.startAnimation(leftAnim);
    }

    public void startRepeatAnim() {
        final Animation hideAnim = AnimationUtils.loadAnimation(mContext, R.anim.anim_alpha_hide);
        final Animation showAnim = AnimationUtils.loadAnimation(mContext, R.anim.anim_alpha_show);

        hideAnim.setAnimationListener(new AnimationListenerImpl() {
            @Override
            public void onAnimationEnd(Animation animation) {
                mInfoView.setText(mContext.getString(R.string.pass_repeat));
                mInfoView.startAnimation(showAnim);
                mPassView.setText(Constants.EMPTY);
            }
        });
        showAnim.setAnimationListener(new AnimationListenerImpl() {
            @Override
            public void onAnimationEnd(Animation animation) {
                mListener.onRepeatAnimEnd();
            }
        });

        mInfoView.startAnimation(hideAnim);
        mPassView.startAnimation(hideAnim);
    }

    public void clearInstance() {
        mContext = null;
        mInfoView = null;
        mPassView = null;
        mListener = null;
    }
}