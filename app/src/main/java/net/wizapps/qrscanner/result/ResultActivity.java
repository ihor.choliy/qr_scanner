package net.wizapps.qrscanner.result;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.google.zxing.Result;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.base.BaseActivity;
import net.wizapps.qrscanner.data.ScanModel;
import net.wizapps.qrscanner.result.mvp.ResultPresenter;
import net.wizapps.qrscanner.result.mvp.ResultPresenterImpl;
import net.wizapps.qrscanner.result.mvp.ResultView;
import net.wizapps.qrscanner.tools.Constants;
import net.wizapps.qrscanner.utils.InfoUtils;
import net.wizapps.qrscanner.utils.IntentUtils;
import net.wizapps.qrscanner.utils.ParseUtils;
import net.wizapps.qrscanner.utils.PrefUtils;
import net.wizapps.qrscanner.utils.ResultUtils;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnLongClick;
import icepick.State;

public class ResultActivity extends BaseActivity implements ResultView {

    @BindView(R.id.textResult) TextView mResult;
    @BindView(R.id.layoutEmail) View mEmail;
    @BindView(R.id.layoutUri) View mUri;
    @BindView(R.id.layoutGeo) View mGeo;
    @BindView(R.id.layoutTel) View mTel;
    @BindView(R.id.layoutSms) View mSms;
    @BindView(R.id.layoutSearch) View mSearch;

    @State boolean mIsCopied;
    @State boolean mIsSaved;

    private final ResultPresenter mPresenter = new ResultPresenterImpl();
    private ScanModel mModel;

    public static Intent newInstance(Context context, Result result) {
        String type = ResultUtils.getType(result);
        String format = String.valueOf(result.getBarcodeFormat());
        Long date = result.getTimestamp();
        String content = result.getText();
        Intent intent = new Intent(context, ResultActivity.class);
        intent.putExtra(Constants.KEY_RESULT, new ScanModel(Constants.EMPTY, type, format, date, content));
        return intent;
    }

    @Override
    protected int layoutRes() {
        return R.layout.activity_result;
    }

    @Override
    protected void setUi() {
        mModel = getIntent().getParcelableExtra(Constants.KEY_RESULT);
        mResult.setText(mModel.getContent());
        checkSettings();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.bind(this);
        mPresenter.checkResultType(mModel.getType());
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.unbind();
    }

    @Override
    public void showEmail() {
        mEmail.setVisibility(View.VISIBLE);
    }

    @Override
    public void showUri() {
        mUri.setVisibility(View.VISIBLE);
    }

    @Override
    public void showGeo() {
        mGeo.setVisibility(View.VISIBLE);
    }

    @Override
    public void showTel() {
        mTel.setVisibility(View.VISIBLE);
    }

    @Override
    public void showSms() {
        mSms.setVisibility(View.VISIBLE);
    }

    @Override
    public void showSearch() {
        mSearch.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError(int textResId) {
        InfoUtils.showAlertError(this, textResId);
    }

    @Override
    public void copyResult() {
        copyResult(Boolean.TRUE);
    }

    @Override
    public void saveResult() {
        saveResult(Boolean.TRUE);
    }

    @OnClick({R.id.resultView, R.id.backButton, R.id.fabCopy, R.id.fabSave, R.id.fabShare, R.id.fabInfo, R.id.fabEmail, R.id.fabUri, R.id.fabGeo, R.id.fabTel, R.id.fabSms, R.id.fabSearch})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.resultView:
                onInfoClick();
                break;
            case R.id.backButton:
                finish();
                break;
            case R.id.fabCopy:
                mPresenter.onCopyClicked(mIsCopied);
                break;
            case R.id.fabSave:
                mPresenter.onSaveClicked(mIsSaved);
                break;
            case R.id.fabShare:
                IntentUtils.shareIntent(this, mModel.getContent());
                break;
            case R.id.fabInfo:
                onInfoClick();
                break;
            case R.id.fabEmail:
                ParseUtils.parseEmail(this, mModel.getContent());
                break;
            case R.id.fabUri:
                IntentUtils.webIntent(this, mModel.getContent());
                break;
            case R.id.fabGeo:
                IntentUtils.geoIntent(this, mModel.getContent());
                break;
            case R.id.fabTel:
                IntentUtils.telIntent(this, mModel.getContent());
                break;
            case R.id.fabSms:
                ParseUtils.parseSms(this, mModel.getContent());
                break;
            case R.id.fabSearch:
                IntentUtils.searchIntent(this, mModel.getContent());
                break;
        }
    }

    @OnLongClick(R.id.resultView)
    boolean onLongClick() {
        mPresenter.onCopyClicked(mIsCopied);
        return Boolean.TRUE;
    }

    private void checkSettings() {
        if (PrefUtils.isAutoCopyOn(this)) copyResult(Boolean.FALSE);
        if (PrefUtils.isAutoSaveOn(this)) saveResult(Boolean.FALSE);
        if (PrefUtils.isAutoLaunchOn(this) && mModel.getType().equals(Constants.TYPE_URI)) {
            IntentUtils.webIntent(this, mModel.getContent().trim());
            finish();
        }
    }

    private void onInfoClick() {
        ResultUtils.showInfo(this, mModel, Boolean.FALSE);
    }

    private void copyResult(boolean showAlert) {
        if (!mIsCopied) mIsCopied = ResultUtils.copyResult(this, mModel.getContent(), showAlert);
    }

    private void saveResult(boolean showAlert) {
        if (!mIsSaved) mIsSaved = ResultUtils.saveResult(this, mModel, showAlert);
    }
}