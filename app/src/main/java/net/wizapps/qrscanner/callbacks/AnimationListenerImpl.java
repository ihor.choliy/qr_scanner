package net.wizapps.qrscanner.callbacks;

import android.view.animation.Animation;

public abstract class AnimationListenerImpl implements Animation.AnimationListener {

    @Override
    public void onAnimationStart(Animation animation) {}

    @Override
    public void onAnimationRepeat(Animation animation) {}
}