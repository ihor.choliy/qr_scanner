package net.wizapps.qrscanner.donate.fragments;

import android.view.View;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.base.BaseFragment;
import net.wizapps.qrscanner.dialogs.WalletDialog;
import net.wizapps.qrscanner.tools.Events;

import org.greenrobot.eventbus.EventBus;

import butterknife.OnClick;

public class DonateInfoFragment extends BaseFragment {

    @Override
    protected int layoutRes() {
        return R.layout.fragment_donate_info;
    }

    @OnClick({R.id.buttonMoney, R.id.buttonBitcoin, R.id.buttonEth, R.id.buttonEtc})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonMoney:
                EventBus.getDefault().post(new Events.DonateAmountEvent());
                break;
            case R.id.buttonBitcoin:
                showWallet(R.string.donate_btc_text, R.string.donate_address_btc, R.drawable.ic_bitcoin);
                break;
            case R.id.buttonEth:
                showWallet(R.string.donate_eth_text, R.string.donate_address_eth, R.drawable.ic_ethereum);
                break;
            case R.id.buttonEtc:
                showWallet(R.string.donate_etc_text, R.string.donate_address_etc, R.drawable.ic_ethereum);
                break;
        }
    }

    private void showWallet(int typeResId, int addressResId, int iconResId) {
        WalletDialog.newInstance(typeResId, addressResId, iconResId).show(getChildFragmentManager(), WalletDialog.class.getSimpleName());
    }
}