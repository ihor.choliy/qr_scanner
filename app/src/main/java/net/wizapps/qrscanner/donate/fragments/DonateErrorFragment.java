package net.wizapps.qrscanner.donate.fragments;

import android.os.Handler;
import android.view.View;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.base.BaseFragment;
import net.wizapps.qrscanner.tools.Events;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;

public class DonateErrorFragment extends BaseFragment {

    public static final String TAG = DonateErrorFragment.class.getSimpleName();

    @BindView(R.id.progressBar) View mProgress;

    @Override
    protected int layoutRes() {
        return R.layout.fragment_donate_error;
    }

    @OnClick(R.id.refreshButton)
    public void onClick() {
        mProgress.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                EventBus.getDefault().post(new Events.DonateAmountEvent());
                mProgress.setVisibility(View.GONE);
            }
        }, getResources().getInteger(R.integer.duration_progress));
    }
}