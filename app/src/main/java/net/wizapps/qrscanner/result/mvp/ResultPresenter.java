package net.wizapps.qrscanner.result.mvp;

import net.wizapps.qrscanner.base.BasePresenter;

public interface ResultPresenter extends BasePresenter<ResultView> {

    void checkResultType(String resultType);

    void onCopyClicked(boolean isCopied);

    void onSaveClicked(boolean isSaved);
}