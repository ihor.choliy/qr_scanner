package net.wizapps.qrscanner.utils;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.tapadoo.alerter.Alerter;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.tools.Constants;

import es.dmoral.toasty.Toasty;

public final class InfoUtils {

    private static Toast sToast;

    private InfoUtils() {}

    public static void showAlertInfo(Context context, int titleResId) {
        alertBuilder(context, null, titleResId, Boolean.FALSE);
    }

    public static void showAlertError(Context context, int textResId) {
        alertBuilder(context, null, textResId, Boolean.TRUE);
    }

    public static void showAlertError(Context context, String text) {
        alertBuilder(context, text, Constants.ZERO, Boolean.TRUE);
    }

    private static void alertBuilder(Context context, String text, int textResId, boolean isError) {
        Alerter.create((AppCompatActivity) context)
                .setTitle(TextUtils.isEmpty(text) ? context.getString(textResId) : text)
                .setDuration(context.getResources().getInteger(R.integer.duration_alert))
                .setIcon(isError ? R.drawable.ic_error : R.drawable.ic_info)
                .setBackgroundColorRes(isError ? R.color.colorRed : R.color.colorAccent)
                .enableSwipeToDismiss()
                .show();
    }

    public static void showSnack(Context context, int textResId) {
        Snackbar.make(getView(context), textResId, Snackbar.LENGTH_SHORT).show();
    }

    public static Snackbar getSnack(Context context, int textResId) {
        return Snackbar.make(getView(context), textResId, Snackbar.LENGTH_LONG);
    }

    public static void showError(Context context, int textResId) {
        toastBuilder(context, textResId, Boolean.TRUE);
    }

    public static void showInfo(Context context, int textResId) {
        toastBuilder(context, textResId, Boolean.FALSE);
    }

    private static View getView(Context context) {
        return ((AppCompatActivity) context).findViewById(android.R.id.content);
    }

    private static void toastBuilder(Context context, int textResId, boolean errorToast) {
        if (sToast != null) sToast.cancel();
        if (errorToast) {
            sToast = Toasty.error(context, context.getString(textResId), Toast.LENGTH_LONG);
        } else {
            sToast = Toasty.info(context, context.getString(textResId));
        }
        sToast.show();
    }
}