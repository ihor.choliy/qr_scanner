package net.wizapps.qrscanner.callbacks;

public interface OnNumberClickListener {

    void onNumberClick(int number);

    void onClearClick();
}