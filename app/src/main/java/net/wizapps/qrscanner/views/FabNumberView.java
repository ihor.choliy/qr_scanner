package net.wizapps.qrscanner.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.tools.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FabNumberView extends FrameLayout {

    @BindView(R.id.numberFab) TextView mNumber;

    public FabNumberView(@NonNull Context context) {
        super(context);
    }

    public FabNumberView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        View view = View.inflate(getContext(), R.layout.view_fab_number, this);
        ButterKnife.bind(this, view);
        setupNumberAttribute(attrs);
    }

    private void setupNumberAttribute(AttributeSet attrs) {
        TypedArray attributes = getContext().obtainStyledAttributes(
                attrs,
                R.styleable.FabNumberView,
                Constants.ZERO,
                Constants.ZERO);
        try {
            mNumber.setText(attributes.getString(R.styleable.FabNumberView_number));
        } finally {
            attributes.recycle();
        }
    }
}