package net.wizapps.qrscanner.utils;

import android.content.Context;

import net.wizapps.qrscanner.tools.Constants;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ParseUtils {

    private ParseUtils() {}

    public static void parseEmail(Context context, String emailResult) {
        String email = Constants.EMPTY;
        String subject = Constants.EMPTY;
        String body = Constants.EMPTY;
        Pattern pattern;
        Matcher matcher;

        // find email
        pattern = Pattern.compile(Constants.PARSE_EMAIL + Constants.PARSE_FIND + Constants.PARSE_SUBJECT);
        matcher = pattern.matcher(emailResult);
        while (matcher.find()) email = matcher.group(1);

        // find subject
        pattern = Pattern.compile(Constants.PARSE_SUBJECT + Constants.PARSE_FIND + Constants.PARSE_BODY);
        matcher = pattern.matcher(emailResult);
        while (matcher.find()) subject = matcher.group(1);

        // find body
        pattern = Pattern.compile(Constants.PARSE_BODY + Constants.PARSE_FIND + Constants.PARSE_END);
        matcher = pattern.matcher(emailResult);
        while (matcher.find()) body = matcher.group(1);

        IntentUtils.emailIntent(context, email, subject, body);
    }

    public static void parseSms(Context context, String smsResult) {
        String phoneNumber = smsResult.split(Constants.PARSE_SMS)[1];
        String smsBody = smsResult.split(Constants.PARSE_SMS)[2];
        IntentUtils.smsIntent(context, phoneNumber, smsBody);
    }
}