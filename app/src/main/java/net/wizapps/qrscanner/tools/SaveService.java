package net.wizapps.qrscanner.tools;

import android.app.IntentService;
import android.content.Intent;

import net.wizapps.qrscanner.data.ScanLab;
import net.wizapps.qrscanner.data.ScanModel;

import java.util.List;

public class SaveService extends IntentService {

    public SaveService() {
        super(SaveService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        boolean suchCodeAlreadyExist = Boolean.FALSE;
        ScanModel result = intent.getParcelableExtra(Constants.KEY_SAVE);
        List<ScanModel> history = ScanLab.getInstance(getApplicationContext()).getHistory();
        for (ScanModel model : history) {
            if (result.getContent().equals(model.getContent())) {
                suchCodeAlreadyExist = Boolean.TRUE;
                break;
            }
        }

        if (!suchCodeAlreadyExist) {
            ScanLab.getInstance(getApplicationContext()).addItem(result, Boolean.FALSE);
        }
    }
}