package net.wizapps.qrscanner.result.mvp;

public interface ResultView {

    void showEmail();

    void showUri();

    void showGeo();

    void showTel();

    void showSms();

    void showSearch();

    void showError(int textResId);

    void copyResult();

    void saveResult();
}