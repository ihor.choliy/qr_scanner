package net.wizapps.qrscanner.data;

import android.provider.BaseColumns;

interface ScanContract extends BaseColumns {

    String TABLE_HISTORY = "table_history";
    String COLUMN_NAME = "column_name";
    String COLUMN_TYPE = "column_type";
    String COLUMN_FORMAT = "column_format";
    String COLUMN_DATE = "column_date";
    String COLUMN_CONTENT = "column_content";
}