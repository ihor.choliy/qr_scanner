package net.wizapps.qrscanner.donate;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.base.BaseActivity;
import net.wizapps.qrscanner.donate.fragments.DonateAmountFragment;
import net.wizapps.qrscanner.donate.fragments.DonateErrorFragment;
import net.wizapps.qrscanner.donate.fragments.DonateInfoFragment;
import net.wizapps.qrscanner.tools.Events;
import net.wizapps.qrscanner.utils.NetworkUtils;

import org.greenrobot.eventbus.Subscribe;

import butterknife.OnClick;

public class DonateActivity extends BaseActivity {

    @Override
    protected int layoutRes() {
        return R.layout.activity_donate;
    }

    @Override
    protected void setUi() {
        addFragment(new DonateInfoFragment());
    }

    @OnClick(R.id.backButton)
    public void onClick() {
        onBackClick();
    }

    @Subscribe
    public void onEvent(Events.DonateAmountEvent event) {
        if (getSupportFragmentManager().findFragmentByTag(DonateErrorFragment.TAG) != null) {
            getSupportFragmentManager().popBackStack();
        }
        if (NetworkUtils.isNetworkConnected(this)) {
            replaceFragment(new DonateAmountFragment(), DonateAmountFragment.TAG);
        } else {
            replaceFragment(new DonateErrorFragment(), DonateErrorFragment.TAG);
        }
    }
}