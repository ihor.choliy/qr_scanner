package net.wizapps.qrscanner.tools;

public interface Constants {

    int ZERO = 0;
    int REQUEST_PERMISSION = 111;
    int ONE_HUNDRED_PERCENT = 100;
    int DONATE_DIALOG_COUNTER = 30;
    int PASSWORD_LENGTH = 4;
    String HIDDEN_PASS = "•";
    String EMPTY = "";

    // Key constants
    String KEY_SAVE = "key_save";
    String KEY_RESULT = "key_result";
    String KEY_IS_HISTORY = "key_is_history";
    String KEY_WALLET_TYPE = "key_wallet_type";
    String KEY_WALLET_ADDRESS = "key_wallet_address";
    String KEY_WALLET_ICON = "key_wallet_icon";

    // Scanner result constants
    String TYPE_CONTACT = "ADDRESSBOOK";
    String TYPE_EMAIL = "EMAIL_ADDRESS";
    String TYPE_PRODUCT = "PRODUCT";
    String TYPE_URI = "URI";
    String TYPE_GEO = "GEO";
    String TYPE_TEL = "TEL";
    String TYPE_SMS = "SMS";
    String TYPE_VIN = "VIN";
    String TYPE_ISBN = "ISBN";
    String TYPE_WIFI = "WIFI";
    String TYPE_CALENDAR = "CALENDAR";

    // Parse constants
    String PARSE_EMAIL = "MATMSG:TO:";
    String PARSE_SUBJECT = ";SUB:";
    String PARSE_BODY = ";BODY:";
    String PARSE_END = ";;";
    String PARSE_FIND = "(.*?)";
    String PARSE_SMS = ":";

    // Date & Time constants
    String DATE_FORMAT = "d MMM yyyy";
    String TIME_FORMAT_UK = "HH:mm";
    String TIME_FORMAT_US = "h:mm a";
    String INFO_DATE_FORMAT_UK = DATE_FORMAT + " (" + TIME_FORMAT_UK + ")";
    String INFO_DATE_FORMAT_US = DATE_FORMAT + " (" + TIME_FORMAT_US + ")";
}