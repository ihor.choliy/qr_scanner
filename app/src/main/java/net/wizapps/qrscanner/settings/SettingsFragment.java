package net.wizapps.qrscanner.settings;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;

import net.wizapps.qrscanner.R;
import net.wizapps.qrscanner.tools.Constants;
import net.wizapps.qrscanner.tools.Events;
import net.wizapps.qrscanner.utils.InfoUtils;
import net.wizapps.qrscanner.utils.PrefUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class SettingsFragment extends PreferenceFragment implements
        SharedPreferences.OnSharedPreferenceChangeListener,
        Preference.OnPreferenceClickListener {

    private SwitchPreference mPrefSound;
    private SwitchPreference mPrefVibrate;
    private SwitchPreference mPrefAutofocus;
    private ListPreference mPrefLaser;
    private ListPreference mPrefWindow;
    private SwitchPreference mPrefCopy;
    private SwitchPreference mPrefSave;
    private SwitchPreference mPrefLaunch;
    private SwitchPreference mPrefDuplicate;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
        setViews();
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        getPreferenceScreen()
                .getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }


    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
        getPreferenceScreen()
                .getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Preference preference = findPreference(key);
        if (preference instanceof ListPreference) {
            ListPreference listPref = (ListPreference) preference;
            String value = sharedPreferences.getString(listPref.getKey(), null);
            setPreferenceSummary(listPref, value);
        }
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        EventBus.getDefault().post(new Events.ShowDialogEvent());
        return Boolean.TRUE;
    }

    @Subscribe
    public void onEvent(Events.RestoreSettingsEvent event) {
        mPrefSound.setChecked(Boolean.TRUE);
        mPrefVibrate.setChecked(Boolean.TRUE);
        mPrefAutofocus.setChecked(Boolean.TRUE);
        mPrefLaser.setValueIndex(Constants.ZERO);
        mPrefWindow.setValueIndex(Constants.ZERO);
        mPrefCopy.setChecked(Boolean.FALSE);
        mPrefSave.setChecked(Boolean.FALSE);
        mPrefLaunch.setChecked(Boolean.FALSE);
        mPrefDuplicate.setChecked(Boolean.FALSE);
        InfoUtils.showAlertInfo(getActivity(), R.string.pref_default_restore);
    }

    private void setViews() {
        mPrefSound = (SwitchPreference) findPreference(getString(R.string.key_flashlight));
        mPrefVibrate = (SwitchPreference) findPreference(getString(R.string.key_vibrate));
        mPrefAutofocus = (SwitchPreference) findPreference(getString(R.string.key_autofocus));
        mPrefLaser = (ListPreference) findPreference(getString(R.string.key_laser));
        mPrefWindow = (ListPreference) findPreference(getString(R.string.key_window_type));
        mPrefCopy = (SwitchPreference) findPreference(getString(R.string.key_copy));
        mPrefSave = (SwitchPreference) findPreference(getString(R.string.key_save));
        mPrefLaunch = (SwitchPreference) findPreference(getString(R.string.key_launch));
        mPrefDuplicate = (SwitchPreference) findPreference(getString(R.string.key_duplicate));
        findPreference(getString(R.string.key_default)).setOnPreferenceClickListener(this);

        String laserColor = PrefUtils.getLaserColor(getActivity());
        String windowType = PrefUtils.getWindowType(getActivity());
        setPreferenceSummary(mPrefLaser, laserColor);
        setPreferenceSummary(mPrefWindow, windowType);
    }

    private void setPreferenceSummary(ListPreference listPref, String value) {
        int prefIndex = listPref.findIndexOfValue(value);
        if (prefIndex >= Constants.ZERO) {
            CharSequence prefLabel = listPref.getEntries()[prefIndex];
            listPref.setSummary(prefLabel);
        }
    }
}