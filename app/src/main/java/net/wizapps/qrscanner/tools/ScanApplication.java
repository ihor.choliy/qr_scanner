package net.wizapps.qrscanner.tools;

import android.app.Application;
import android.support.v4.content.ContextCompat;

import net.wizapps.qrscanner.R;

import es.dmoral.toasty.Toasty;

public class ScanApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        setToasty();
    }

    private void setToasty() {
        Toasty.Config.getInstance()
                .setErrorColor(ContextCompat.getColor(this, R.color.colorRed))
                .setInfoColor(ContextCompat.getColor(this, R.color.colorAccent))
                .setTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
                .tintIcon(Boolean.TRUE)
                .apply();
    }
}