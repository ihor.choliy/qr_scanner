package net.wizapps.qrscanner.data;

import android.database.Cursor;
import android.database.CursorWrapper;

class ScanCursorWrapper extends CursorWrapper {

    ScanCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    ScanModel getHistoryItem() {
        String id = getString(getColumnIndex(ScanContract._ID));
        String name = getString(getColumnIndex(ScanContract.COLUMN_NAME));
        String type = getString(getColumnIndex(ScanContract.COLUMN_TYPE));
        String format = getString(getColumnIndex(ScanContract.COLUMN_FORMAT));
        Long date = getLong(getColumnIndex(ScanContract.COLUMN_DATE));
        String content = getString(getColumnIndex(ScanContract.COLUMN_CONTENT));
        return new ScanModel(id, name, type, format, date, content);
    }
}